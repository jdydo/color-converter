﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ColorModelConversion.ColorModel;
using ColorModelConversion.ColorConversion;

namespace ColorModelConversion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields
        int guardian = 0;
        #endregion

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();

            setUpColorBar();

            WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
            UpdateWebColor(webColor);

            ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
        }
        #endregion

        #region Update
        private void UpdateRGB(RGB rgb)
        {
            RGB_Red_UpDown.Value = (int)Math.Round(rgb.Red, 0);
            RGB_Green_UpDown.Value = (int)Math.Round(rgb.Green, 0);
            RGB_Blue_UpDown.Value = (int)Math.Round(rgb.Blue, 0);
        }

        private void UpdateCMY(CMY cmy)
        {
            CMY_Cyan_UpDown.Value = (int)Math.Round(cmy.Cyan * 100.0, 0);
            CMY_Magenta_UpDown.Value = (int)Math.Round(cmy.Magenta * 100.0, 0);
            CMY_Yellow_UpDown.Value = (int)Math.Round(cmy.Yellow * 100.0, 0);
        }

        private void UpdateCMYK(CMYK cmyk)
        {
            CMYK_Cyan_UpDown.Value = (int)Math.Round(cmyk.Cyan * 100.0, 0);
            CMYK_Magenta_UpDown.Value = (int)Math.Round(cmyk.Magenta * 100.0, 0);
            CMYK_Yellow_UpDown.Value = (int)Math.Round(cmyk.Yellow * 100.0, 0);
            CMYK_Black_UpDown.Value = (int)Math.Round(cmyk.Black * 100.0, 0);
        }

        private void UpdateHSL(HSL hsl)
        {
            HSL_Hue_UpDown.Value = (int)Math.Round(hsl.Hue, 0);
            HSL_Saturation_UpDown.Value = (int)Math.Round(hsl.Saturation * 100.0, 0);
            HSL_Lightness_UpDown.Value = (int)Math.Round(hsl.Lightness * 100.0, 0);
        }

        private void UpdateHSB(HSB hsb)
        {
            HSB_Hue_UpDown.Value = (int)Math.Round(hsb.Hue, 0);
            HSB_Saturation_UpDown.Value = (int)Math.Round(hsb.Saturation * 100.0, 0);
            HSB_Brightness_UpDown.Value = (int)Math.Round(hsb.Brightness * 100.0, 0);
        }

        private void UpdateCIEXYZ(CIEXYZ ciexyz)
        {
            CIEXYZ_X_UpDown.Value = Math.Round(ciexyz.X * 100.0, 2);
            CIEXYZ_Y_UpDown.Value = Math.Round(ciexyz.Y * 100.0, 2);
            CIEXYZ_Z_UpDown.Value = Math.Round(ciexyz.Z * 100.0, 2);
        }

        private void UpdateCIELab(CIELab cielab)
        {
            CIELab_L_UpDown.Value = Math.Round(cielab.L, 2);
            CIELab_a_UpDown.Value = Math.Round(cielab.a, 2);
            CIELab_b_UpDown.Value = Math.Round(cielab.b, 2);
        }

        private void UpdateWebColor(WebColor webColor)
        {
            WebColor_textBox.Text = webColor.Hex;
        }
        #endregion

        #region ValueChanged Events
        private void RGB_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (guardian == 1)
            {
                CMY cmy = RGBtoConversion.RGBtoCMY(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateCMY(cmy);

                CMYK cmyk = RGBtoConversion.RGBtoCMYK(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateCMYK(cmyk);

                HSL hsl = RGBtoConversion.RGBtoHSL(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateHSL(hsl);

                HSB hsb = RGBtoConversion.RGBtoHSB(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateHSB(hsb);

                CIEXYZ ciexyz = RGBtoConversion.RGBtoCIEXYZ(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateCIEXYZ(ciexyz);

                CIELab cielab = RGBtoConversion.RGBtoCIELab(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateCIELab(cielab);

                WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateWebColor(webColor);

                ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
            }
        }

        private void CMY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (guardian == 2)
            {
                RGB rgb = CMYtoConversion.CMYtoRGB(new CMY(((double)CMY_Cyan_UpDown.Value) / 100.0, ((double)CMY_Magenta_UpDown.Value) / 100.0, ((double)CMY_Yellow_UpDown.Value) / 100.0));
                UpdateRGB(rgb);

                CMYK cmyk = CMYtoConversion.CMYtoCMYK(new CMY(((double)CMY_Cyan_UpDown.Value) / 100.0, ((double)CMY_Magenta_UpDown.Value) / 100.0, ((double)CMY_Yellow_UpDown.Value) / 100.0));
                UpdateCMYK(cmyk);

                HSL hsl = CMYtoConversion.CMYtoHSL(new CMY(((double)CMY_Cyan_UpDown.Value) / 100.0, ((double)CMY_Magenta_UpDown.Value) / 100.0, ((double)CMY_Yellow_UpDown.Value) / 100.0));
                UpdateHSL(hsl);

                HSB hsb = CMYtoConversion.CMYtoHSB(new CMY(((double)CMY_Cyan_UpDown.Value) / 100.0, ((double)CMY_Magenta_UpDown.Value) / 100.0, ((double)CMY_Yellow_UpDown.Value) / 100.0));
                UpdateHSB(hsb);

                CIEXYZ ciexyz = CMYtoConversion.CMYtoCIEXYZ(new CMY(((double)CMY_Cyan_UpDown.Value) / 100.0, ((double)CMY_Magenta_UpDown.Value) / 100.0, ((double)CMY_Yellow_UpDown.Value) / 100.0));
                UpdateCIEXYZ(ciexyz);

                CIELab cielab = CMYtoConversion.CMYtoCIELab(new CMY(((double)CMY_Cyan_UpDown.Value) / 100.0, ((double)CMY_Magenta_UpDown.Value) / 100.0, ((double)CMY_Yellow_UpDown.Value) / 100.0));
                UpdateCIELab(cielab);

                WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateWebColor(webColor);

                ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
            }
        }

        private void CMYK_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (guardian == 3)
            {
                RGB rgb = CMYKtoConversion.CMYKtoRGB(new CMYK(((double)CMYK_Cyan_UpDown.Value) / 100.0, ((double)CMYK_Magenta_UpDown.Value) / 100.0, ((double)CMYK_Yellow_UpDown.Value) / 100.0, ((double)CMYK_Black_UpDown.Value) / 100.0));
                UpdateRGB(rgb);

                CMY cmy = CMYKtoConversion.CMYKtoCMY(new CMYK(((double)CMYK_Cyan_UpDown.Value) / 100.0, ((double)CMYK_Magenta_UpDown.Value) / 100.0, ((double)CMYK_Yellow_UpDown.Value) / 100.0, ((double)CMYK_Black_UpDown.Value) / 100.0));
                UpdateCMY(cmy);

                HSL hsl = CMYKtoConversion.CMYKtoHSL(new CMYK(((double)CMYK_Cyan_UpDown.Value) / 100.0, ((double)CMYK_Magenta_UpDown.Value) / 100.0, ((double)CMYK_Yellow_UpDown.Value) / 100.0, ((double)CMYK_Black_UpDown.Value) / 100.0));
                UpdateHSL(hsl);

                HSB hsb = CMYKtoConversion.CMYKtoHSB(new CMYK(((double)CMYK_Cyan_UpDown.Value) / 100.0, ((double)CMYK_Magenta_UpDown.Value) / 100.0, ((double)CMYK_Yellow_UpDown.Value) / 100.0, ((double)CMYK_Black_UpDown.Value) / 100.0));
                UpdateHSB(hsb);

                CIEXYZ ciexyz = CMYKtoConversion.CMYKtoCIEXYZ(new CMYK(((double)CMYK_Cyan_UpDown.Value) / 100.0, ((double)CMYK_Magenta_UpDown.Value) / 100.0, ((double)CMYK_Yellow_UpDown.Value) / 100.0, ((double)CMYK_Black_UpDown.Value) / 100.0));
                UpdateCIEXYZ(ciexyz);

                CIELab cielab = CMYKtoConversion.CMYKtoCIELab(new CMYK(((double)CMYK_Cyan_UpDown.Value) / 100.0, ((double)CMYK_Magenta_UpDown.Value) / 100.0, ((double)CMYK_Yellow_UpDown.Value) / 100.0, ((double)CMYK_Black_UpDown.Value) / 100.0));
                UpdateCIELab(cielab);

                WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateWebColor(webColor);

                ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
            }
        }

        private void HSL_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (guardian == 4)
            {
                RGB rgb = HSLtoConversion.HSLtoRGB(new HSL(((double)HSL_Hue_UpDown.Value), ((double)HSL_Saturation_UpDown.Value) / 100.0, ((double)HSL_Lightness_UpDown.Value) / 100.0));
                UpdateRGB(rgb);

                CMY cmy = HSLtoConversion.HSLtoCMY(new HSL((double)HSL_Hue_UpDown.Value, (double)HSL_Saturation_UpDown.Value / 100.0, (double)HSL_Lightness_UpDown.Value / 100.0));
                UpdateCMY(cmy);

                CMYK cmyk = HSLtoConversion.HSLtoCMYK(new HSL((double)HSL_Hue_UpDown.Value, (double)HSL_Saturation_UpDown.Value / 100.0, (double)HSL_Lightness_UpDown.Value / 100.0));
                UpdateCMYK(cmyk);

                HSB hsb = HSLtoConversion.HSLtoHSB(new HSL((double)HSL_Hue_UpDown.Value, (double)HSL_Saturation_UpDown.Value / 100.0, (double)HSL_Lightness_UpDown.Value / 100.0));
                UpdateHSB(hsb);

                CIEXYZ ciexyz = HSLtoConversion.HSLtoCIEXYZ(new HSL((double)HSL_Hue_UpDown.Value, (double)HSL_Saturation_UpDown.Value / 100.0, (double)HSL_Lightness_UpDown.Value / 100.0));
                UpdateCIEXYZ(ciexyz);

                CIELab cielab = HSLtoConversion.HSLtoCIELab(new HSL((double)HSL_Hue_UpDown.Value, (double)HSL_Saturation_UpDown.Value / 100.0, (double)HSL_Lightness_UpDown.Value / 100.0));
                UpdateCIELab(cielab);

                WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateWebColor(webColor);

                ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
            }
        }

        private void HSB_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (guardian == 5)
            {
                RGB rgb = HSBtoConversion.HSBtoRGB(new HSB(((double)HSB_Hue_UpDown.Value), ((double)HSB_Saturation_UpDown.Value) / 100.0, ((double)HSB_Brightness_UpDown.Value) / 100.0));
                UpdateRGB(rgb);

                CMY cmy = HSBtoConversion.HSBtoCMY(new HSB(((double)HSB_Hue_UpDown.Value), ((double)HSB_Saturation_UpDown.Value) / 100.0, ((double)HSB_Brightness_UpDown.Value) / 100.0));
                UpdateCMY(cmy);

                CMYK cmyk = HSBtoConversion.HSBtoCMYK(new HSB(((double)HSB_Hue_UpDown.Value), ((double)HSB_Saturation_UpDown.Value) / 100.0, ((double)HSB_Brightness_UpDown.Value) / 100.0));
                UpdateCMYK(cmyk);

                HSL hsl = HSBtoConversion.HSBtoHSL(new HSB(((double)HSB_Hue_UpDown.Value), ((double)HSB_Saturation_UpDown.Value) / 100.0, ((double)HSB_Brightness_UpDown.Value) / 100.0));
                UpdateHSL(hsl);

                CIEXYZ ciexyz = HSBtoConversion.HSBtoCIEXYZ(new HSB(((double)HSB_Hue_UpDown.Value), ((double)HSB_Saturation_UpDown.Value) / 100.0, ((double)HSB_Brightness_UpDown.Value) / 100.0));
                UpdateCIEXYZ(ciexyz);

                CIELab cielab = HSBtoConversion.HSBtoCIELab(new HSB(((double)HSB_Hue_UpDown.Value), ((double)HSB_Saturation_UpDown.Value) / 100.0, ((double)HSB_Brightness_UpDown.Value) / 100.0));
                UpdateCIELab(cielab);

                WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateWebColor(webColor);

                ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
            }
        }

        private void CIEXYZ_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (guardian == 6)
            {
                RGB rgb = CIEXYZtoConversion.CIEXYZtoRGB(new CIEXYZ(((double)CIEXYZ_X_UpDown.Value) / 100.0, ((double)CIEXYZ_Y_UpDown.Value) / 100.0, ((double)CIEXYZ_Z_UpDown.Value) / 100.0));
                UpdateRGB(rgb);

                CMY cmy = CIEXYZtoConversion.CIEXYZtoCMY(new CIEXYZ(((double)CIEXYZ_X_UpDown.Value) / 100.0, ((double)CIEXYZ_Y_UpDown.Value) / 100.0, ((double)CIEXYZ_Z_UpDown.Value) / 100.0));
                UpdateCMY(cmy);

                CMYK cmyk = CIEXYZtoConversion.CIEXYZtoCMYK(new CIEXYZ(((double)CIEXYZ_X_UpDown.Value) / 100.0, ((double)CIEXYZ_Y_UpDown.Value) / 100.0, ((double)CIEXYZ_Z_UpDown.Value) / 100.0));
                UpdateCMYK(cmyk);

                HSL hsl = CIEXYZtoConversion.CIEXYZtoHSL(new CIEXYZ(((double)CIEXYZ_X_UpDown.Value) / 100.0, ((double)CIEXYZ_Y_UpDown.Value) / 100.0, ((double)CIEXYZ_Z_UpDown.Value) / 100.0));
                UpdateHSL(hsl);

                HSB hsb = CIEXYZtoConversion.CIEXYZtoHSB(new CIEXYZ(((double)CIEXYZ_X_UpDown.Value) / 100.0, ((double)CIEXYZ_Y_UpDown.Value) / 100.0, ((double)CIEXYZ_Z_UpDown.Value) / 100.0));
                UpdateHSB(hsb);

                CIELab cielab = CIEXYZtoConversion.CIEXYZtoCIELab(new CIEXYZ(((double)CIEXYZ_X_UpDown.Value) / 100.0, ((double)CIEXYZ_Y_UpDown.Value) / 100.0, ((double)CIEXYZ_Z_UpDown.Value) / 100.0));
                UpdateCIELab(cielab);

                WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateWebColor(webColor);

                ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
            }
        }

        private void CIELab_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (guardian == 7)
            {
                RGB rgb = CIELabtoConversion.CIELabtoRGB(new CIELab(((double)CIELab_L_UpDown.Value), ((double)CIELab_a_UpDown.Value), ((double)CIELab_b_UpDown.Value)));
                UpdateRGB(rgb);

                CMY cmy = CIELabtoConversion.CIELabtoCMY(new CIELab(((double)CIELab_L_UpDown.Value), ((double)CIELab_a_UpDown.Value), ((double)CIELab_b_UpDown.Value)));
                UpdateCMY(cmy);

                CMYK cmyk = CIELabtoConversion.CIELabtoCMYK(new CIELab(((double)CIELab_L_UpDown.Value), ((double)CIELab_a_UpDown.Value), ((double)CIELab_b_UpDown.Value)));
                UpdateCMYK(cmyk);

                HSL hsl = CIELabtoConversion.CIELabtoHSL(new CIELab(((double)CIELab_L_UpDown.Value), ((double)CIELab_a_UpDown.Value), ((double)CIELab_b_UpDown.Value)));
                UpdateHSL(hsl);

                HSB hsb = CIELabtoConversion.CIELabtoHSB(new CIELab(((double)CIELab_L_UpDown.Value), ((double)CIELab_a_UpDown.Value), ((double)CIELab_b_UpDown.Value)));
                UpdateHSB(hsb);

                CIEXYZ ciexyz = CIELabtoConversion.CIELabtoCIEXYZ(new CIELab(((double)CIELab_L_UpDown.Value), ((double)CIELab_a_UpDown.Value), ((double)CIELab_b_UpDown.Value)));
                UpdateCIEXYZ(ciexyz);

                WebColor webColor = RGBtoConversion.RGBToWebColor(new RGB((double)RGB_Red_UpDown.Value, (double)RGB_Green_UpDown.Value, (double)RGB_Blue_UpDown.Value));
                UpdateWebColor(webColor);

                ColorLabel.Background = new SolidColorBrush(Color.FromRgb((byte)RGB_Red_UpDown.Value, (byte)RGB_Green_UpDown.Value, (byte)RGB_Blue_UpDown.Value));
            }
        }
        #endregion

        #region MouseEnter Event
        private void RGB_MouseEnter(object sender, MouseEventArgs e)
        {
            guardian = 1;
        }

        private void CMY_MouseEnter(object sender, MouseEventArgs e)
        {
            guardian = 2;
        }

        private void CMYK_MouseEnter(object sender, MouseEventArgs e)
        {
            guardian = 3;
        }

        private void HSL_MouseEnter(object sender, MouseEventArgs e)
        {
            guardian = 4;
        }

        private void HSB_MouseEnter(object sender, MouseEventArgs e)
        {
            guardian = 5;
        }

        private void CIEXYZ_MouseEnter(object sender, MouseEventArgs e)
        {
            guardian = 6;
        }

        private void CIELab_MouseEnter(object sender, MouseEventArgs e)
        {
            guardian = 7;
        }
        #endregion

        #region SetUp Color Tiles
        private void setUpColorBar()
        {
            Color1.Background = new SolidColorBrush(Color.FromRgb((byte)255, (byte)255, (byte)0));
            Color2.Background = new SolidColorBrush(Color.FromRgb((byte)255, (byte)128, (byte)0));
            Color3.Background = new SolidColorBrush(Color.FromRgb((byte)255, (byte)0, (byte)0));
            Color4.Background = new SolidColorBrush(Color.FromRgb((byte)0, (byte)255, (byte)0));
            Color5.Background = new SolidColorBrush(Color.FromRgb((byte)0, (byte)128, (byte)0));
            Color6.Background = new SolidColorBrush(Color.FromRgb((byte)0, (byte)255, (byte)255));
            Color7.Background = new SolidColorBrush(Color.FromRgb((byte)0, (byte)0, (byte)255));
            Color8.Background = new SolidColorBrush(Color.FromRgb((byte)170, (byte)0, (byte)255));
            Color9.Background = new SolidColorBrush(Color.FromRgb((byte)255, (byte)0, (byte)255)); 
            Color10.Background = new SolidColorBrush(Color.FromRgb((byte)153, (byte)102, (byte)0));
            Color11.Background = new SolidColorBrush(Color.FromRgb((byte)255, (byte)255, (byte)255));
            Color12.Background = new SolidColorBrush(Color.FromRgb((byte)243, (byte)243, (byte)243));
            Color13.Background = new SolidColorBrush(Color.FromRgb((byte)179, (byte)179, (byte)179));
            Color14.Background = new SolidColorBrush(Color.FromRgb((byte)103, (byte)103, (byte)103));
            Color15.Background = new SolidColorBrush(Color.FromRgb((byte)0, (byte)0, (byte)0));
        }
        #endregion

        #region Color Tiles MouseDown Events
        private void Color1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 255;
            RGB_Green_UpDown.Value = 255;
            RGB_Blue_UpDown.Value = 0;
        }

        private void Color2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 255;
            RGB_Green_UpDown.Value = 128;
            RGB_Blue_UpDown.Value = 0;
        }

        private void Color3_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 255;
            RGB_Green_UpDown.Value = 0;
            RGB_Blue_UpDown.Value = 0;
        }

        private void Color4_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 0;
            RGB_Green_UpDown.Value = 255;
            RGB_Blue_UpDown.Value = 0;
        }

        private void Color5_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 0;
            RGB_Green_UpDown.Value = 128;
            RGB_Blue_UpDown.Value = 0;
        }

        private void Color6_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 0;
            RGB_Green_UpDown.Value = 255;
            RGB_Blue_UpDown.Value = 255;
        }

        private void Color7_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 0;
            RGB_Green_UpDown.Value = 0;
            RGB_Blue_UpDown.Value = 255;
        }

        private void Color8_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 170;
            RGB_Green_UpDown.Value = 0;
            RGB_Blue_UpDown.Value = 255;
        }

        private void Color9_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 255;
            RGB_Green_UpDown.Value = 0;
            RGB_Blue_UpDown.Value = 255;
        }

        private void Color10_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 153;
            RGB_Green_UpDown.Value = 102;
            RGB_Blue_UpDown.Value = 0;
        }

        private void Color11_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 255;
            RGB_Green_UpDown.Value = 255;
            RGB_Blue_UpDown.Value = 255;
        }

        private void Color12_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 243;
            RGB_Green_UpDown.Value = 243;
            RGB_Blue_UpDown.Value = 243;
        }

        private void Color13_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 179;
            RGB_Green_UpDown.Value = 179;
            RGB_Blue_UpDown.Value = 179;
        }

        private void Color14_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 103;
            RGB_Green_UpDown.Value = 103;
            RGB_Blue_UpDown.Value = 103;
        }

        private void Color15_MouseDown(object sender, MouseButtonEventArgs e)
        {
            guardian = 1;
            RGB_Red_UpDown.Value = 0;
            RGB_Green_UpDown.Value = 0;
            RGB_Blue_UpDown.Value = 0;
        }
        #endregion
    }
}
