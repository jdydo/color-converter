﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorModelConversion.ColorModel;

namespace ColorModelConversion.ColorConversion
{
    public static class CIELabtoConversion
    {
        public static RGB CIELabtoRGB(CIELab cielab)
        {
            return CIEXYZtoConversion.CIEXYZtoRGB(CIELabtoConversion.CIELabtoCIEXYZ(cielab));
        }

        public static CMY CIELabtoCMY(CIELab cielab)
        {
            return CIEXYZtoConversion.CIEXYZtoCMY(CIELabtoConversion.CIELabtoCIEXYZ(cielab));
        }

        public static CMYK CIELabtoCMYK(CIELab cielab)
        {
            return CIEXYZtoConversion.CIEXYZtoCMYK(CIELabtoConversion.CIELabtoCIEXYZ(cielab));
        }

        public static HSB CIELabtoHSB(CIELab cielab)
        {
            return CIEXYZtoConversion.CIEXYZtoHSB(CIELabtoConversion.CIELabtoCIEXYZ(cielab));
        }

        public static HSL CIELabtoHSL(CIELab cielab)
        {
            return CIEXYZtoConversion.CIEXYZtoHSL(CIELabtoConversion.CIELabtoCIEXYZ(cielab));
        }

        public static CIEXYZ CIELabtoCIEXYZ(CIELab cielab)
        {
            double y = (cielab.L + 16.0) / 116.0;
            double x = cielab.a / 500.0 + y;
            double z = y - cielab.b / 200.0;

            x = (Math.Pow(x, 3) <= 0.008856) ? ( x - 16.0 / 116.0 ) / 7.787 : (Math.Pow(x, 3));
            y = (Math.Pow(y, 3) <= 0.008856) ? ( y - 16.0 / 116.0 ) / 7.787 : (Math.Pow(y, 3));
            z = (Math.Pow(z, 3) <= 0.008856) ? ( z - 16.0 / 116.0 ) / 7.787 : (Math.Pow(z, 3));

            double ref_x =  0.95047;
            double ref_y = 1.00000;
            double ref_z = 1.08883;

            return new CIEXYZ(ref_x * x, ref_y * y, ref_z * z);
        }
    }
}
