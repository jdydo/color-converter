﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorModelConversion.ColorModel;

namespace ColorModelConversion.ColorConversion
{
    public static class RGBtoConversion
    {
        public static CMY RGBtoCMY(RGB rgb)
        {
            double cyan = 1.0 - (rgb.Red / 255.0);
            double magenta = 1.0 - (rgb.Green / 255.0);
            double yellow = 1.0 - (rgb.Blue / 255.0);

            return new CMY(cyan, magenta, yellow);
        }

        public static CMYK RGBtoCMYK(RGB rgb)
        {
            double nRed = rgb.Red / 255.0;
            double nGreen = rgb.Green / 255.0;
            double nBlue = rgb.Blue / 255.0;

            double black = Math.Min(1.0 - nBlue, Math.Min(1.0 - nRed, 1.0 - nGreen));
            double cyan = (1.0 - nRed - black) / (1.0 - black);
            double magenta = (1.0 - nGreen - black) / (1.0 - black);
            double yellow = (1.0 - nBlue - black) / (1.0 - black);

            return new CMYK(cyan, magenta, yellow, black);
        }

        public static HSB RGBtoHSB(RGB rgb)
        {
            double nRed = Math.Round(rgb.Red) / 255.0;
            double nGreen = Math.Round(rgb.Green) / 255.0;
            double nBlue = Math.Round(rgb.Blue) / 255.0;

            double brightness = Math.Max(nRed, Math.Max(nGreen, nBlue));
            double min = Math.Min(nRed, Math.Min(nGreen, nBlue));

            double hue = 0.0;
            if (brightness == nRed && nGreen >= nBlue)
            {
                hue = 60 * (nGreen - nBlue) / (brightness - min);
            }
            else if (brightness == nRed && nGreen < nBlue)
            {
                hue = 60 * (nGreen - nBlue) / (brightness - min) + 360;
            }
            else if (brightness == nGreen)
            {
                hue = 60 * (nBlue - nRed) / (brightness - min) + 120;
            }
            else if (brightness == nBlue)
            {
                hue = 60 * (nRed - nGreen) / (brightness - min) + 240;
            }

            double saturation = (brightness == 0) ? 0.0 : (1.0 - (min / brightness));

            return new HSB(hue, saturation, brightness);
        }

        public static HSL RGBtoHSL(RGB rgb)
        {
            double hue = 0.0;
            double saturation = 0.0;
            double lightness = 0.0;

            double nRed = Math.Round(rgb.Red) / 255.0;
            double nGreen = Math.Round(rgb.Green) / 255.0;
            double nBlue = Math.Round(rgb.Blue) / 255.0;

            double max = Math.Max(nRed, Math.Max(nGreen, nBlue));
            double min = Math.Min(nRed, Math.Min(nGreen, nBlue));

            if (max == min)
            {
                hue = 0.0;
            }
            else if (max == nRed && nGreen >= nBlue)
            {
                hue = 60.0 * (nGreen - nBlue) / (max - min);
            }
            else if (max == nRed && nGreen < nBlue)
            {
                hue = 60.0 * (nGreen - nBlue) / (max - min) + 360.0;
            }
            else if (max == nGreen)
            {
                hue = 60.0 * (nBlue - nRed) / (max - min) + 120.0;
            }
            else if (max == nBlue)
            {
                hue = 60.0 * (nRed - nGreen) / (max - min) + 240.0;
            }

            lightness = (max + min) / 2.0;

            if (lightness == 0 || max == min)
            {
                saturation = 0.0;
            }
            else if (0 < lightness && lightness <= 0.5)
            {
                saturation = (max - min) / (max + min);
            }
            else if (lightness > 0.5)
            {
                saturation = (max - min) / (2.0 - (max + min));
            }

            return new HSL(hue, saturation, lightness);
        }

        public static CIEXYZ RGBtoCIEXYZ(RGB rgb)
        {
            double nRed = rgb.Red / 255.0;
            double nGreen = rgb.Green / 255.0;
            double nBlue = rgb.Blue / 255.0;

            double nRed1 = (nRed > 0.04045) ? Math.Pow((nRed + 0.055) / (1.0 + 0.055), 2.4) : (nRed / 12.92);
            double nGreen1 = (nGreen > 0.04045) ? Math.Pow((nGreen + 0.055) / (1.0 + 0.055), 2.4) : (nGreen / 12.92);
            double nBlue1 = (nBlue > 0.04045) ? Math.Pow((nBlue + 0.055) / (1.0 + 0.055), 2.4) : (nBlue / 12.92);

            double x = nRed1 * 0.4124 + nGreen1 * 0.3576 + nBlue1 * 0.1805;
            double y = nRed1 * 0.2126 + nGreen1 * 0.7152 + nBlue1 * 0.0722;
            double z = nRed1 * 0.0193 + nGreen1 * 0.1192 + nBlue1 * 0.9505;

            return new CIEXYZ(x, y, z);
        }

        public static WebColor RGBToWebColor(RGB rgb)
        {
            return new WebColor(String.Format("#{0:x2}{1:x2}{2:x2}", (int)rgb.Red, (int)rgb.Green, (int)rgb.Blue).ToUpper());
        }

        public static CIELab RGBtoCIELab(RGB rgb)
        {
            return CIEXYZtoConversion.CIEXYZtoCIELab(RGBtoConversion.RGBtoCIEXYZ(rgb));
        }
    }
}
