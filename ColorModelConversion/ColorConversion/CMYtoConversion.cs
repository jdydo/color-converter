﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorModelConversion.ColorModel;

namespace ColorModelConversion.ColorConversion
{
    public static class CMYtoConversion
    {
        public static RGB CMYtoRGB(CMY cmy)
        {
            double red = (1.0 - cmy.Cyan) * 255.0;
            double green = (1.0 - cmy.Magenta) * 255.0;
            double blue = (1.0 - cmy.Yellow) * 255.0;

            return new RGB(red, green, blue);
        }

        public static CMYK CMYtoCMYK(CMY cmy)
        {
            double black = 1.0;

            if (cmy.Cyan < black)
                black = cmy.Cyan;

            if (cmy.Magenta < black)
                black = cmy.Magenta;

            if (cmy.Yellow < black)
                black = cmy.Yellow;

            double cyan = 0.0;
            double magenta = 0.0;
            double yellow = 0.0;

            if (black != 1.0)
            {
                cyan = (cmy.Cyan - black) / (1.0 - black);
                magenta = (cmy.Magenta - black) / (1.0 - black);
                yellow = (cmy.Yellow - black) / (1.0 - black);
            }

            return new CMYK(cyan, magenta, yellow, black);
        }

        public static HSB CMYtoHSB(CMY cmy)
        {
            return RGBtoConversion.RGBtoHSB(CMYtoConversion.CMYtoRGB(cmy));
        }

        public static HSL CMYtoHSL(CMY cmy)
        {
            return RGBtoConversion.RGBtoHSL(CMYtoConversion.CMYtoRGB(cmy));
        }

        public static CIEXYZ CMYtoCIEXYZ(CMY cmy)
        {
            return RGBtoConversion.RGBtoCIEXYZ(CMYtoConversion.CMYtoRGB(cmy));
        }

        public static CIELab CMYtoCIELab(CMY cmy)
        {
            return RGBtoConversion.RGBtoCIELab(CMYtoConversion.CMYtoRGB(cmy));
        }
    }
}
