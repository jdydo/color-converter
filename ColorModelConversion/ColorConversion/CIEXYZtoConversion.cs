﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorModelConversion.ColorModel;

namespace ColorModelConversion.ColorConversion
{
    public static class CIEXYZtoConversion
    {
        public static RGB CIEXYZtoRGB(CIEXYZ ciexyz)
        {
            double red = ciexyz.X * 3.2406 - ciexyz.Y * 1.5372 - ciexyz.Z * 0.4986; 
            double green = -ciexyz.X * 0.9689 + ciexyz.Y * 1.8758 + ciexyz.Z * 0.0415; 
            double blue = ciexyz.X * 0.0557 - ciexyz.Y * 0.2040 + ciexyz.Z * 1.0570; 

            red = (red <= 0.0031308) ? 12.92 * red : (1.0 + 0.055) * Math.Pow(red, (1.0 / 2.4)) - 0.055;
            green = (green <= 0.0031308) ? 12.92 * green : (1.0 + 0.055) * Math.Pow(green, (1.0 / 2.4)) - 0.055;
            blue = (blue <= 0.0031308) ? 12.92 * blue : (1.0 + 0.055) * Math.Pow(blue, (1.0 / 2.4)) - 0.055;

            return new RGB(red * 255.0, green * 255.0, blue * 255.0);
        }

        public static CMY CIEXYZtoCMY(CIEXYZ ciexyz)
        {
            return RGBtoConversion.RGBtoCMY(CIEXYZtoConversion.CIEXYZtoRGB(ciexyz));
        }

        public static CMYK CIEXYZtoCMYK(CIEXYZ ciexyz)
        {
            return RGBtoConversion.RGBtoCMYK(CIEXYZtoConversion.CIEXYZtoRGB(ciexyz));
        }

        public static HSB CIEXYZtoHSB(CIEXYZ ciexyz)
        {
            RGB rgb = CIEXYZtoConversion.CIEXYZtoRGB(ciexyz);
            return RGBtoConversion.RGBtoHSB(new RGB(Math.Round(rgb.Red), Math.Round(rgb.Green), Math.Round(rgb.Blue)));
        }

        public static HSL CIEXYZtoHSL(CIEXYZ ciexyz)
        {
            RGB rgb = CIEXYZtoConversion.CIEXYZtoRGB(ciexyz);
            return RGBtoConversion.RGBtoHSL(new RGB(Math.Round(rgb.Red), Math.Round(rgb.Green), Math.Round(rgb.Blue)));
        }

        public static CIELab CIEXYZtoCIELab(CIEXYZ ciexyz)
        {
            double ref_x = 0.95047;
            double ref_y = 1.00000;
            double ref_z = 1.08883;

            double x = ciexyz.X / ref_x;
            double y = ciexyz.Y / ref_y;
            double z = ciexyz.Z / ref_z;

            x = (x <= 0.008856) ? (7.787 * x) + (16.0 / 116.0) : Math.Pow(x, 1.0 / 3.0);
            y = (y <= 0.008856) ? (7.787 * y) + (16.0 / 116.0) : Math.Pow(y, 1.0 / 3.0);
            z = (z <= 0.008856) ? (7.787 * z) + (16.0 / 116.0) : Math.Pow(z, 1.0 / 3.0);

            double L = (116.0 * y) - 16.0;
            double a = 500.0 * (x - y);
            double b = 200.0 * (y - z);

            return new CIELab(L, a, b);
        }
    }
}
