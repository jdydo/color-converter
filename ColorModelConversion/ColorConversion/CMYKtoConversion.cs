﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorModelConversion.ColorModel;

namespace ColorModelConversion.ColorConversion
{
    public static class CMYKtoConversion
    {
        public static RGB CMYKtoRGB(CMYK cmyk)
        {
            double red = (1.0 - cmyk.Cyan) * (1.0 - cmyk.Black) * 255.0;
            double green = (1.0 - cmyk.Magenta) * (1.0 - cmyk.Black) * 255.0;
            double blue = (1.0 - cmyk.Yellow) * (1.0 - cmyk.Black) * 255.0;

            return new RGB(red, green, blue);
        }

        public static CMY CMYKtoCMY(CMYK cmyk)
        {
            double cyan = (cmyk.Cyan * (1.0 - cmyk.Black) + cmyk.Black);
            double magenta = (cmyk.Magenta * (1.0 - cmyk.Black) + cmyk.Black);
            double yellow = (cmyk.Yellow * (1.0 - cmyk.Black) + cmyk.Black);

            return new CMY(cyan, magenta, yellow);
        }

        public static HSB CMYKtoHSB(CMYK cmyk)
        {
            return RGBtoConversion.RGBtoHSB(CMYKtoConversion.CMYKtoRGB(cmyk));
        }

        public static HSL CMYKtoHSL(CMYK cmyk)
        {
            return RGBtoConversion.RGBtoHSL(CMYKtoConversion.CMYKtoRGB(cmyk));
        }

        public static CIEXYZ CMYKtoCIEXYZ(CMYK cmyk)
        {
            return RGBtoConversion.RGBtoCIEXYZ(CMYKtoConversion.CMYKtoRGB(cmyk));
        }

        public static CIELab CMYKtoCIELab(CMYK cmyk)
        {
            return RGBtoConversion.RGBtoCIELab(CMYKtoConversion.CMYKtoRGB(cmyk));
        }
    }
}
