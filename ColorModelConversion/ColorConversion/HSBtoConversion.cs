﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorModelConversion.ColorModel;

namespace ColorModelConversion.ColorConversion
{
    public static class HSBtoConversion
    {
        public static RGB HSBtoRGB(HSB hsb)
        {
            double r = 0.0;
            double g = 0.0;
            double b = 0.0;

            if(hsb.Saturation == 0.0)
            {
                r = g = b = hsb.Brightness;
            }
            else
            {
                double sectorPos = hsb.Hue / 60.0;
                int sectorNumber = (int)(Math.Floor(sectorPos));
                double fractionalSector = sectorPos - sectorNumber;

                double p = hsb.Brightness * (1.0 - hsb.Saturation);
                double q = hsb.Brightness * (1.0 - (hsb.Saturation * fractionalSector));
                double t = hsb.Brightness * (1.0 - (hsb.Saturation * (1.0 - fractionalSector)));

                switch(sectorNumber)
                {
                    case 0:
                        r = hsb.Brightness;
                        g = t;
                        b = p;
                        break;
                    case 1:
                        r = q;
                        g = hsb.Brightness;
                        b = p;
                        break;
                    case 2:
                        r = p;
                        g = hsb.Brightness;
                        b = t;
                        break;
                    case 3:
                        r = p;
                        g = q;
                        b = hsb.Brightness;
                        break;
                    case 4:
                        r = t;
                        g = p;
                        b = hsb.Brightness;
                        break;
                    case 5:
                        r = hsb.Brightness;
                        g = p;
                        b = q;
                        break;
                }
            }

            return new RGB(Math.Round(r * 255.0, 0), Math.Round(g * 255.0, 0), Math.Round(b * 255.0, 0));
        }

        public static CMY HSBtoCMY(HSB hsb)
        {
            return RGBtoConversion.RGBtoCMY(HSBtoConversion.HSBtoRGB(hsb));
        }

        public static CMYK HSBtoCMYK(HSB hsb)
        {
            return RGBtoConversion.RGBtoCMYK(HSBtoConversion.HSBtoRGB(hsb));
        }

        public static HSL HSBtoHSL(HSB hsb)
        {
            return RGBtoConversion.RGBtoHSL(HSBtoConversion.HSBtoRGB(hsb));
        }

        public static CIEXYZ HSBtoCIEXYZ(HSB hsb)
        {
            return RGBtoConversion.RGBtoCIEXYZ(HSBtoConversion.HSBtoRGB(hsb));
        }

        public static CIELab HSBtoCIELab(HSB hsb)
        {
            return RGBtoConversion.RGBtoCIELab(HSBtoConversion.HSBtoRGB(hsb));
        }
    }
}
