﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ColorModelConversion.ColorModel;

namespace ColorModelConversion.ColorConversion
{
    public static class HSLtoConversion
    {
        public static RGB HSLtoRGB(HSL hsl)
        {
            if (hsl.Saturation == 0)
            {
                return new RGB(hsl.Lightness * 255.0, hsl.Lightness * 255.0, hsl.Lightness * 255.0);
            }
            else
            {
                double q = (hsl.Lightness < 0.5) ? (hsl.Lightness * (1.0 + hsl.Saturation)) : (hsl.Lightness + hsl.Saturation - (hsl.Lightness * hsl.Saturation));
                double p = (2.0 * hsl.Lightness) - q;

                double Hk = hsl.Hue / 360.0;
                double[] T = new double[3];
                T[0] = Hk + (1.0 / 3.0);    // Tr
                T[1] = Hk;                // Tb
                T[2] = Hk - (1.0 / 3.0);    // Tg

                for (int i = 0; i < 3; i++)
                {
                    if (T[i] < 0) T[i] += 1.0;
                    if (T[i] > 1) T[i] -= 1.0;

                    if ((T[i] * 6) < 1)
                    {
                        T[i] = p + ((q - p) * 6.0 * T[i]);
                    }
                    else if ((T[i] * 2.0) < 1) //(1.0/6.0)<=T[i] && T[i]<0.5
                    {
                        T[i] = q;
                    }
                    else if ((T[i] * 3.0) < 2) // 0.5<=T[i] && T[i]<(2.0/3.0)
                    {
                        T[i] = p + (q - p) * ((2.0 / 3.0) - T[i]) * 6.0;
                    }
                    else T[i] = p;
                }

                return new RGB(T[0] * 255.0, T[1] * 255.0, T[2] * 255.0);
            }
        }

        public static CMY HSLtoCMY(HSL hsl)
        {
            return RGBtoConversion.RGBtoCMY(HSLtoConversion.HSLtoRGB(hsl));
        }

        public static CMYK HSLtoCMYK(HSL hsl)
        {
            return RGBtoConversion.RGBtoCMYK(HSLtoConversion.HSLtoRGB(hsl));
        }

        public static HSB HSLtoHSB(HSL hsl)
        {
            return RGBtoConversion.RGBtoHSB(HSLtoConversion.HSLtoRGB(hsl));
        }

        public static CIEXYZ HSLtoCIEXYZ(HSL hsl)
        {
            return RGBtoConversion.RGBtoCIEXYZ(HSLtoConversion.HSLtoRGB(hsl));
        }

        public static CIELab HSLtoCIELab(HSL hsl)
        {
            return RGBtoConversion.RGBtoCIELab(HSLtoConversion.HSLtoRGB(hsl));
        }
    }
}
