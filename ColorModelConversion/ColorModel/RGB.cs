﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorModelConversion.ColorModel
{
    public class RGB
    {
        #region Fields
        private double red;
        private double green;
        private double blue;
        #endregion

        #region Properties
        public double Red
        {
            get { return this.red; }
            set { this.red = CheckRange(value); }
        }

        public double Green
        {
            get { return this.green; }
            set { this.green = CheckRange(value); }
        }

        public double Blue
        {
            get { return this.blue; }
            set { this.blue = CheckRange(value); }
        }
        #endregion

        #region Range Check
        double CheckRange(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 255.0) ? 255.0 : (double.IsNaN(value)) ? 0.0 : value;
        }
        #endregion

        #region Constructor
        public RGB(double Red, double Green, double Blue)
        {
            this.Red = Red;
            this.Green = Green;
            this.Blue = Blue;
        }
        #endregion
    }
}
