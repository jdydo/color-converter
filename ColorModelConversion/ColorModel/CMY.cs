﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorModelConversion.ColorModel
{
    public class CMY
    {
        #region Fields
        private double cyan;
        private double magenta;
        private double yellow;
        #endregion

        #region Properties
        public double Cyan
        {
            get { return this.cyan; }
            set { this.cyan = CheckRange(value); }
        }

        public double Magenta
        {
            get { return this.magenta; }
            set { this.magenta = CheckRange(value); }
        }

        public double Yellow
        {
            get { return this.yellow; }
            set { this.yellow = CheckRange(value); }
        }
        #endregion

        #region Range Checker
        double CheckRange(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 1.0) ? 1.0 : (double.IsNaN(value)) ? 0.0 : value;
        }
        #endregion

        #region Constructor
        public CMY(double Cyan, double Magenta, double Yellow)
        {
            this.Cyan = Cyan;
            this.Magenta = Magenta;
            this.Yellow = Yellow;
        }        
        #endregion
    }
}
