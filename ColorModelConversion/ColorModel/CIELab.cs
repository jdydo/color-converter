﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorModelConversion.ColorModel
{
    public class CIELab
    {
        public double L;
        public double a;
        public double b;

        public CIELab(double L, double a, double b)
        {
            this.L = L;
            this.a = a;
            this.b = b;
        }
    }
}
