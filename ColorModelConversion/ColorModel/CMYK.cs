﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorModelConversion.ColorModel
{
    public class CMYK
    {
        #region Fields
        private double cyan;
        private double magenta;
        private double yellow;
        private double black;
        #endregion

        #region Properties
        public double Cyan
        {
            get { return this.cyan; }
            set { this.cyan = CheckRange(value); }
        }

        public double Magenta
        {
            get { return this.magenta; }
            set { this.magenta = CheckRange(value); }
        }

        public double Yellow
        {
            get { return this.yellow; }
            set { this.yellow = CheckRange(value); }
        }

        public double Black
        {
            get { return this.black; }
            set { this.black = CheckRange(value); }
        }
        #endregion

        #region Range Checker
        double CheckRange(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 1.0) ? 1.0 : (double.IsNaN(value)) ? 0.0 : value;
        }
        #endregion

        #region Constructor
        public CMYK(double Cyan, double Magenta, double Yellow, double Black)
        {
            this.Cyan = Cyan;
            this.Magenta = Magenta;
            this.Yellow = Yellow;
            this.Black = Black;
        }        
        #endregion
    }
}
