﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorModelConversion.ColorModel
{
    public class HSL
    {
        #region Fields
        private double hue;
        private double saturation;
        private double lightness;
        #endregion

        #region Properties
        public double Hue
        {
            get { return this.hue; }
            set { this.hue = CheckRangeHue(value); }
        }

        public double Saturation
        {
            get { return this.saturation; }
            set { this.saturation = CheckRange(value); }
        }

        public double Lightness
        {
            get { return this.lightness; }
            set { this.lightness = CheckRange(value); }
        }
        #endregion

        #region Range Check
        double CheckRange(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 1.0) ? 1.0 : (double.IsNaN(value)) ? 0.0 : value;
        }

        double CheckRangeHue(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 359.0) ? 359.0 : (double.IsNaN(value)) ? 0.0 : value;
        }
        #endregion

        #region Constructor
        public HSL(double Hue, double Saturation, double Lightness)
        {
            this.Hue = Hue;
            this.Saturation = Saturation;
            this.Lightness = Lightness;
        }
        #endregion
    }
}
