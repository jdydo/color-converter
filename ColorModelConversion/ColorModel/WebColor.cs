﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorModelConversion.ColorModel
{
    public class WebColor
    {
        public string Hex;

        public WebColor(string Hex)
        {
            this.Hex = Hex;
        }
    }
}
