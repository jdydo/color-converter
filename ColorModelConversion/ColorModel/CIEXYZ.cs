﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorModelConversion.ColorModel
{
    public class CIEXYZ
    {
        #region Fields
        public double x;
        public double y;
        public double z;
        #endregion

        #region Properties
        public double X
        {
            get { return this.x; }
            set { this.x = CheckRangeX(value); }
        }

        public double Y
        {
            get { return this.y; }
            set { this.y = CheckRangeY(value); }
        }

        public double Z
        {
            get { return this.z; }
            set { this.z = CheckRangeZ(value); }
        }
        #endregion

        #region Range Checker
        double CheckRangeX(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 0.95047) ? 0.95047 : (double.IsNaN(value)) ? 0.0 : value;
        }

        double CheckRangeY(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 1.00000) ? 1.00000 : (double.IsNaN(value)) ? 0.0 : value;
        }

        double CheckRangeZ(double value)
        {
            return (value < 0.0) ? 0.0 : (value > 1.08883) ? 1.08883 : (double.IsNaN(value)) ? 0.0 : value;
        }
        #endregion

        #region Constructor
        public CIEXYZ(double X, double Y, double Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }
        #endregion
    }
}
